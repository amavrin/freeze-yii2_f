<?php

namespace app\models;

use Yii;
use yii\base\Model;

use \app\Models\User;
 
/**
 * Transfer form
 */
class TransferForm extends Model
{
 
    public $email;
	public $value;
 

    public function rules()
    {
        return [
            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 64],
            ['email', 'validateUserByEmail'],
            ['value', 'required'],
			['value', 'integer'],
			['value', 'string', 'max' => 32],
        ];
    }
	
	
    public function validateUserByEmail()
    {
		if (!$user = User::findOne(['email' => $this->email])) {
			$this->addError('email', 'Пользователь с таким email не найден');
		}
    }
 

    public function attributeLabels()
    {
        return [
            'email' => 'Email получателя',
            'value' => 'Сумма перевода',
        ];
    }

 
}