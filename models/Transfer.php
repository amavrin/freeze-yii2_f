<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
 

class Transfer extends ActiveRecord
{
	
    const STATUS_DEACTIVE = 0;
    const STATUS_ACTIVE = 1;


    public static function tableName()
    {
        return '{{%transfer}}';
    }
 

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    public function getCommission()
    {
        $commission = null;
        if ($rlCommission = RlTransferCommission::findOne(['transfer_id' => $this->id])) {
            $commission = $rlCommission->getCommission();
        }
        return $commission;
    }

    public function getResultWithCommission()
    {
		$value_transfer = $this->value;
        if ($commission = $this->getCommission()) {
            /* @var \app\Models\Commission $commission*/
            $scenario = $commission->getPropertyOrderStep();
            if (count($scenario) > 0) {
                $value_commis = 0;
                foreach ($scenario as $scenarioStepModel) {
                    if ($func = Scenario::$property_result_func[$scenarioStepModel->property->code]) {
                        $result = call_user_func_array(array($scenarioStepModel, $func), array($this, $value_transfer, $value_commis));
                        $value_transfer = $result['value_transfer'];
                        $value_commis = $result['value_commis'];
                    }
                }
            }
        }
        return $value_transfer;
    }


    public function getDateCreate($format = 'd.m.Y H:i')
    {
        $date = '';
        if ($this->created_at > '0000-00-00 00:00:00') {
            $created_at = new \DateTime($this->created_at);
            $date = $created_at->format($format);
        }
        return $date;
    }

    public function getDateUpdate($format = 'd.m.Y H:i')
    {
        $date = '';
        if ($this->updated_at > '0000-00-00 00:00:00') {
            $updated_at = new \DateTime($this->updated_at);
            $date = $updated_at->format($format);
        }
        return $date;
    }

}