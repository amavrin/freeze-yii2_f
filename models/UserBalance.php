<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
 

class UserBalance extends ActiveRecord
{

    public static function tableName()
    {
        return '{{%user_balance}}';
    }
 

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    public function getDateCreate($format = 'd.m.Y H:i')
    {
        $date = '';
        if ($this->created_at > '0000-00-00 00:00:00') {
            $created_at = new \DateTime($this->created_at);
            $date = $created_at->format($format);
        }
        return $date;
    }

    public function getDateUpdate($format = 'd.m.Y H:i')
    {
        $date = '';
        if ($this->updated_at > '0000-00-00 00:00:00') {
            $updated_at = new \DateTime($this->updated_at);
            $date = $updated_at->format($format);
        }
        return $date;
    }
 
}