<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
 

class RlTransferCommission extends ActiveRecord
{

    public static function tableName()
    {
        return '{{%rl_transfer_commission}}';
    }

    public function getCommission()
    {
        return Commission::findOne(['id' => $this->commission_id]);
    }
}