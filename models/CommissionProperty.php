<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
 

class CommissionProperty extends ActiveRecord
{

    const PERCENT_CODE = 'PERCENT';
    const MIN_SUMM_TRANSFER_CODE = 'MIN_SUMM_TRANSFER';
    const MAX_SUMM_TRANSFER_CODE = 'MAX_SUMM_TRANSFER';
    const MIN_VALUE_COMMIS_CODE = 'MIN_VALUE_COMMIS';

    public static function tableName()
    {
        return '{{%commission_property}}';
    }

}