<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use app\models\CommissionProperty;
 

class Scenario extends ActiveRecord
{
    const STATUS_DEACTIVE = 0;
    const STATUS_ACTIVE = 1;

    public static $property_result_func = [
        CommissionProperty::PERCENT_CODE => 'getPercentResult',
        CommissionProperty::MIN_SUMM_TRANSFER_CODE => '',
        CommissionProperty::MAX_SUMM_TRANSFER_CODE => '',
        CommissionProperty::MIN_VALUE_COMMIS_CODE => 'getMinValueCommisResult',
    ];

    public static function tableName()
    {
        return '{{%scenario}}';
    }
 

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }
	
	public function getProperty()
	{
		return $this->hasOne(CommissionProperty::className(), ['id' => 'commission_property_id']);
	}

    public function getPercentResult(\app\Models\Transfer $transfer=null, $new_value_transfer=0, $value_commis=0)
    {
        if ($this->value) {
			$value_commis = $new_value_transfer * $this->value;
            $new_value_transfer = $new_value_transfer + $value_commis;
        }
        return [
			'value_transfer' => $new_value_transfer,
			'value_commis' => $value_commis
		];
    }

    public function getMinValueCommisResult(\app\Models\Transfer $transfer, $new_value_transfer=0, $value_commis=0)
    {
		if ($this->value) {
			if ($value_commis < $this->value) {
				$new_value_commis = $this->value;
			}
		}
        $new_value_transfer = $transfer->value + $new_value_commis;
		
        return [
			'value_transfer' => $new_value_transfer,
			'value_commis' => $value_commis
		];
    }

    public function getDateCreate($format = 'd.m.Y H:i')
    {
        $date = '';
        if ($this->created_at > '0000-00-00 00:00:00') {
            $created_at = new \DateTime($this->created_at);
            $date = $created_at->format($format);
        }
        return $date;
    }

    public function getDateUpdate($format = 'd.m.Y H:i')
    {
        $date = '';
        if ($this->updated_at > '0000-00-00 00:00:00') {
            $updated_at = new \DateTime($this->updated_at);
            $date = $updated_at->format($format);
        }
        return $date;
    }

}