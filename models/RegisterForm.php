<?php

namespace app\models;

use Yii;
use yii\base\Model;
 
/**
 * Signup form
 */
class RegisterForm extends Model
{
 
    public $username;
    public $email;
    public $password;
    public $verifyCode;
 

    public function rules()
    {
        return [
            ['username', 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => '\app\models\User', 'message' => 'Указанный логин уже используется в системе.'],
            ['username', 'string', 'min' => 2, 'max' => 64],
            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\app\models\User', 'message' => 'Указанный email уже используется в системе.'],
            ['password', 'required'],
            ['password', 'string', 'min' => 6, 'max' => 64],
			['verifyCode', 'captcha', 'message' => 'Каптча введена неверно'],
        ];
    }
 

    public function attributeLabels()
    {
        return [
            'username' => 'Логин',
            'password' => 'Пароль',
            'verifyCode' => 'Каптча',
        ];
    }
 

    public function signup()
    {
        $user = new User();
        $user->username = $this->username;
        $user->email = $this->email;
        $user->setPassword($this->password);
        return $user->save() ? $user : null;
    }
 
}