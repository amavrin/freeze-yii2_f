<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
 

class RlUserCommission extends ActiveRecord
{

    public static function tableName()
    {
        return '{{%rl_user_commission}}';
    }

    public function getCommission()
    {
        return Commission::findOne(['id' => $this->commission_id]);
    }
}