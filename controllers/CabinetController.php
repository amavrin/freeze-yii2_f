<?php

namespace app\controllers;

use app\models\Commission;
use app\models\Transfer;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii\web\HttpException;
use yii\bootstrap\ActiveForm;

use app\models\User;
use app\models\UserBalance;
use app\models\TransferForm;
use yii\helpers\Html;
use app\models\RlTransferCommission;
use app\models\RlUserCommission;

class CabinetController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['*'],
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }


    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }


    public function actionIndex()
    {
        $user = User::findOne(Yii::$app->user->identity->getId());
        if ($user) {
            $balance_value = '0.00 y.e.';
            $balance_date = '';
            if ($balance = $user->getBalance()) {
                /* @var \app\Models\UserBalance $balance */
                $balance_value = $balance->value . ' y.e.';
                $balance_date = $balance->getDateCreate();
                if ($date_update = $balance->getDateUpdate()) {
                    $balance_date = $date_update;
                }
            }
			$commission = false;
			$user_commis_desc = '';
			if ($rl_user_commission = RlUserCommission::findOne(['user_id' => $user->id])) {
				$commission = $rl_user_commission->getCommission();
				$user_commis_desc = $commission->description;
			}
			
            $transferFormModel = new TransferForm();

            if ($transferFormModel->load(Yii::$app->request->post()) && Yii::$app->request->isAjax) {

                $result = [];
                //Yii::$app->response->format = Response::FORMAT_JSON; 
                $transferFormModel->validate();
                if($transferFormModel->hasErrors()) {
                    foreach ($transferFormModel->getErrors() as $attribute => $errors) {
                        $result[Html::getInputId($transferFormModel, $attribute)] = $errors;
                    }
                    return $this->asJson(['validation' => $result]);
                }

                $errors = 0;
                if ($recipient_user = User::findOne(['email' => $transferFormModel->email])) {
                    $transfer = new Transfer;
                    $transfer->sender_user_id = $user->id;
                    $transfer->value = $transferFormModel->value;
                    $transfer->recipient_user_id = $recipient_user->id;
                    if ($transfer->save()) {
						if ($commission) {
							$rl_transfer_commis = new RlTransferCommission;
							$rl_transfer_commis->transfer_id = $transfer->id;
							$rl_transfer_commis->commission_id = $commission->id;
							if (!$rl_transfer_commis->save()) {
								$errors++;
							}
						} else {
							$errors++;
						}
                        if ($errors === 0) {
                            // С учётом комиссии
                            $valueWithCom = $transfer->getResultWithCommission();

                            $sender_user_balance = UserBalance::findOne(['user_id' => $user->id]);
                            $sender_user_balance->value = $sender_user_balance->value - $valueWithCom;
                            if (!$sender_user_balance->save()) {
                                Yii::$app->session->setFlash('error', 'Возникли ошибки при проведении операции.');
								return $this->refresh();

                            }
                            $recipient_user_balance = UserBalance::findOne(['user_id' => $recipient_user->id]);
                            $recipient_user_balance->value = $recipient_user_balance->value + $transferFormModel->value;
                            if (!$recipient_user_balance->save()) {
                                Yii::$app->session->setFlash('error', 'Возникли ошибки при проведении операции.');
								return $this->refresh();
                            }
							
							$transfer->total_value = $valueWithCom;
							$transfer->status = Transfer::STATUS_ACTIVE;
							if ($transfer->save()) {
								Yii::$app->session->setFlash('success', 'Операция успешно произведена.');
								return $this->asJson(['success' => true]);

							} else {
								Yii::$app->session->setFlash('error', 'Возникли ошибки при проведении операции.');
								return $this->refresh();
							}
							
                        } else {
                            Yii::$app->session->setFlash('error', 'Возникли ошибки при проведении операции.');
							return $this->refresh();
                        }
                    } else {
                        Yii::$app->session->setFlash('error', 'Возникли ошибки при проведении операции.');
						return $this->refresh();
                    }
                } else {
                    Yii::$app->session->setFlash('error', 'Возникли ошибки при проведении операции.');
					return $this->refresh();
                }
            }

            return $this->render('index', [
                'user' => $user,
                'user_balance_value' => $balance_value,
                'user_balance_date' => $balance_date,
                'transferFormModel' => $transferFormModel,
				'user_commis_desc' => $user_commis_desc
            ]);
        }
        throw new HttpException(404 ,'User not found');

    }

    public function actionCommissionResult($transfer_value)
    {
        $user = User::findOne(Yii::$app->user->identity->getId());
        if ($user) {
			
        }
    }

}
