<?php

/* @var $this yii\web\View */
use yii\helpers\Url;

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Добро пожаловать!</h1>
		<? if (Yii::$app->user->isGuest) { ?>
			<p class="lead">Вы можете стать нашим партнёром прямо сейчас.</p>
			<p><a class="btn btn-lg btn-success" href="<?=Url::to(['/register'])?>">Регистрация</a></p>
		<? } else { ?>
			<p><a class="btn btn-lg btn-success" href="<?=Url::to(['/cabinet/index'])?>">Личный кабинет</a></p>
		<? } ?>
    </div>

    <div class="body-content">

    </div>
</div>
