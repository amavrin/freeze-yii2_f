<?php

/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use yii\bootstrap\ActiveForm;


$this->title = 'Личный кабинет';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-index">
	<h1><?= Html::encode($this->title) ?></h1>
    <div class="body-content">
		<div class="row">
            <div class="col-lg-5">
				<h3>Профиль пользователя</h3>
				<?= DetailView::widget([
					'model' => $user,
					'attributes' => [
						[
							'label' => 'Логин',
							'value' => $user->username,
						],
						[
							'label' => 'Email',
							'value' => $user->email,
						],
					],
				]) ?>
			</div>
            <div class="col-lg-5">
				<h3>Финансовая информация</h3>
				<?= DetailView::widget([
					'model' => false,
					'attributes' => [
						[
							'label' => 'Баланс',
							'value' => $user_balance_value,
						],
						[
							'label' => 'Последнее изменение',
							'value' => $user_balance_date,
						],
					],
				]) ?>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-5">
				<h3>Отправить денежные средства</h3>
				
				<p>Перевод облагается комиссией: <?=$user_commis_desc;?></p>

                <?php $form = ActiveForm::begin(['id' => 'transfer-form', 'method' => 'POST', 'action' => '/cabinet/index', 'encodeErrorSummary' => false]); ?>
					
                    <?= $form->field($transferFormModel, 'email') ?>
					
					<?= $form->field($transferFormModel, 'value') ?>

                    <div class="form-group">
                        <?= Html::submitButton('Отправить', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
                    </div>

                <?php ActiveForm::end(); ?>
			</div>
		</div>
		
    </div>
	
</div>
<?php
$js = <<<JS
$('#transfer-form').on('beforeSubmit', function () {
    var yiiform = $(this);
    $.ajax({
            type: yiiform.attr('method'),
            url: yiiform.attr('action'),
            data: yiiform.serializeArray(),
        }
    )
        .done(function(data) {
            if(data.success) {
                // data is saved
				location.reload();
            } else if (data.validation) {
                // server validation failed
                yiiform.yiiActiveForm('updateMessages', data.validation, true); // renders validation messages at appropriate places
            } else {
                // incorrect server response
				location.reload();
            }
        })
        .fail(function () {
            // request failed
        })

    return false; // prevent default form submission
})
JS;
$this->registerJs($js);
?>
